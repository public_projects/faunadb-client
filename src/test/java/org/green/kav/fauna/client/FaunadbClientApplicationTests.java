package org.green.kav.fauna.client;

import org.green.kav.fauna.client.model.DeploymentModel;
import org.green.kav.fauna.client.model.DeploymentResultSet;
import org.green.kav.fauna.client.repository.DeploymentRepository;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import java.util.concurrent.ExecutionException;

@SpringBootTest
class FaunadbClientApplicationTests {
	private final static Logger log = LoggerFactory.getLogger(FaunadbClientApplicationTests.class);
	@Autowired
	private DeploymentRepository deploymentRepository;

	@Test
	public void getByEnvironment() throws ExecutionException, InterruptedException {
		log.info("{}", deploymentRepository.getByEnvironment("alpha1"));
	}

	@Test
	void getAll() throws Exception {
		deploymentRepository.getAllDeployment().forEach((DeploymentModel deploymentModel) -> {
			log.info("{}", deploymentModel.toString());
		});
	}

	@Test
	void getById() throws ExecutionException, InterruptedException {
		log.info("{}", deploymentRepository.getById("324740765589701195").toString());
	}

	@Test
	void create() throws ExecutionException, InterruptedException {
		DeploymentModel deploymentModel = new DeploymentModel();
		deploymentModel.setDirectory("D:\\Users\\sm13\\Documents\\development\\ipf\\be\\branch_5.6.x\\emerald\\distribution\\target\\assembly\\system");
		deploymentModel.setEnvironment("local-5.6.x");
		deploymentModel.setIpAddress("localhost");
		deploymentModel.setStatus("ACTIVE");
		deploymentModel.setType("LOCAL");
		deploymentModel.setTempDir("");

		deploymentRepository.create(deploymentModel);
	}

	@Test
	public void delete() throws ExecutionException, InterruptedException {
		deploymentRepository.deleteById("322489730054751825");
	}

	@Test
	void update() throws ExecutionException, InterruptedException {
		DeploymentModel deploymentModel = new DeploymentModel();
		deploymentModel.setDirectory("/opt/alpha1/test");
		deploymentModel.setEnvironment("alpha1");
		deploymentModel.setIpAddress("192.168.1.111");
		deploymentModel.setId("322489823084413521");

		deploymentRepository.update(deploymentModel);
	}
}
