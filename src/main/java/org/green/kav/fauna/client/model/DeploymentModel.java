package org.green.kav.fauna.client.model;

public class DeploymentModel {
    private String id;
    private String environment;
    private String ipAddress;
    private String directory;
    private String type;
    private String status;
    private String tempDir;

    public DeploymentModel(){
    }

    public String getEnvironment() {
        return environment;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getDirectory() {
        return directory;
    }

    public void setDirectory(String directory) {
        this.directory = directory;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTempDir() {
        return tempDir;
    }

    public void setTempDir(String tempDir) {
        this.tempDir = tempDir;
    }

    @Override
    public String toString() {
        return "DeploymentModel{" + "id='" + id + '\'' + ", environment='" + environment + '\'' + ", ipAddress='" + ipAddress + '\'' +
               ", directory='" + directory + '\'' + ", type='" + type + '\'' + ", status='" + status + '\'' + ", tempDir='" + tempDir +
               '\'' + '}';
    }
}
