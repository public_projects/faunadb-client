package org.green.kav.fauna.client.repository;

import com.faunadb.client.FaunaClient;
import com.faunadb.client.query.Language;
import static com.faunadb.client.query.Language.*;
import static com.faunadb.client.query.Language.Var;
import com.faunadb.client.types.Value;
import org.green.kav.fauna.client.model.DeploymentModel;
import org.green.kav.fauna.client.model.DeploymentResultSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.concurrent.ExecutionException;

@Service
public class DeploymentRepository {
    private final static Logger log = LoggerFactory.getLogger(DeploymentRepository.class);
    private final static String COLLECTION = "deployment";
    private final static String GET_ALL_INDEX = "getAllDeployment";
    private final static String GET_BY_ENVIRONMENT = "getByEnvironment";

    private final FaunaClient client;

    @Autowired
    public DeploymentRepository(FaunaClient faunaClient){
        this.client = faunaClient;
    }

    /**
     * FQL
     * ---
     *
     * Map(
     *        Paginate(Match(Index("getByEnvironment"), "alpha1")),
     *        Lambda("deployment",
     *          Let(
     *            {
     *              doc: Get(Var("deployment"))
     *            },
     *            {
     *              id: Select(["ref", "id"], Var("doc")),
     *              environment: Select(["data", "environment"], Var("doc")),
     *              directory: Select(["data", "directory"], Var("doc")),
     *              ipAdd: Select(["data", "ipAdd"], Var("doc"))
     *            }
     *          )
     *        )
     *      )
     * @param environment
     * @return
     */
    public DeploymentModel getByEnvironment(String environment) throws ExecutionException, InterruptedException {
        log.info("{}", client.query(Map(Paginate(Match(Index(GET_BY_ENVIRONMENT), Value(environment))), Lambda("x", Get(Var("x"))))).get());

        Value newValue = client.query(SelectAsIndex(Path("data"),
                                                    Map(Paginate(Match(Index(Language.Value(GET_BY_ENVIRONMENT)), Value(environment))),
                                                        Lambda(Language.Value(COLLECTION),
                                                               Let("doc", Get(Var("deployment")))
                                                                       .in(
                                                                               Obj(
                                                                                       "id", Select(Path("ref", "id"), Var("doc")),
                                                                                       "environment", Select(Path("data", "environment"), Var("doc")),
                                                                                       "directory", Select(Path("data", "directory"), Var("doc")),
                                                                                       "ipAddress", Select(Path("data", "ipAdd"), Var("doc"))
                                                                                  )
                                                                          )
                                                              )
                                                       )
                                                   )
                                     )
                               .get();

        List<DeploymentModel> models = List.copyOf(newValue.collect(DeploymentModel.class));
        if (models == null || models.isEmpty()) return null;
        if (models.size() >1) throw new  ExecutionException(new Exception("Found " + models.size() + "Records"));

        return models.get(0);
    }

    /**
     * FQL
     * ---
     * Map(
     *   Paginate(Match(Index("getAllDeployment"))),
     *   Lambda("deployment",
     *     Let(
     *       {
     *         doc: Get(Var("deployment"))
     *       },
     *       {
     *         id: Select(["ref", "id"], Var("doc")),
     *         environment: Select(["data", "environment"], Var("doc")),
     *         directory: Select(["data", "directory"], Var("doc")),
     *         ipAdd: Select(["data", "ipAdd"], Var("doc"))
     *       }
     *     )
     *   )
     * )
     *
     * @return
     */
    public List<DeploymentModel> getAllDeployment() throws ExecutionException, InterruptedException {
        Value deployments = client.query(SelectAsIndex(Path("data"),
                                                           Map(
                                                                   Paginate(Match(Index(Language.Value(GET_ALL_INDEX)))),
                                                                   Lambda(
                                                                           Language.Value(COLLECTION),
                                                                           Let("doc", Get(Var("deployment")))
                                                                                   .in(
                                                                                           Obj(
                                                                                           "id", Select(Path("ref", "id"), Var("doc")),
                                                                                           "environment", Select(Path("data", "environment"), Var("doc")),
                                                                                           "directory", Select(Path("data", "directory"), Var("doc")),
                                                                                           "ipAddress", Select(Path("data", "ipAdd"), Var("doc"))
                                                                                              )
                                                                                      )
                                                                         )
                                                              )
                                                          )
                                            ).get();
        return List.copyOf(deployments.collect(DeploymentModel.class));
    }

    /**
     * FQL
     * ---
     * Let(
     *   {
     *     doc: Get(Ref(Collection("deployment"), "322460886119744075")),
     *   },
     *   {
     *     id: Select(["ref", "id"], Var("doc")),
     *     environment: Select(["data", "environment"], Var("doc")),
     *     ipAdd: Select(["data", "ipAdd"], Var("doc")),
     *     directory: Select(["data", "directory"], Var("doc"))
     *   }
     * )
     * @param id
     * @return
     * @throws ExecutionException
     * @throws InterruptedException
     */
    public DeploymentResultSet getById(String id) throws ExecutionException, InterruptedException {
//        log.info("{}", client.query(Get(Ref(Collection(COLLECTION), id))).get());

        Value newValue = client.query(Let("doc", Get(Ref(Collection(COLLECTION), id)))
                                              .in(
                                                      Obj(
                                                              "id", Select(Path("ref", "id"), Var("doc")),
                                                              "deployment", Select(Path("data"), Var("doc"))
                                                         )
                                                 )
                                     ).get();

        return newValue.to(DeploymentResultSet.class).get();
    }

    /**
     * FQL
     *
     * Create("deployment", {
     *   data: {
     *     environment: "legacy1",
     *     ipAdd: "192.168.1.106",
     *     directory: "/opt/legacy/test"
     *   }
     * })
     *
     * reference: https://docs.fauna.com/fauna/current/tutorials/crud.html?lang=shell#post
     *
     * @param newRecord
     * @throws ExecutionException
     * @throws InterruptedException
     */
    public void create(DeploymentModel newRecord) throws ExecutionException, InterruptedException {
        client.query(
                Create(
                        Collection(Value(COLLECTION)),
                        Obj("data", Value(newRecord))
                      )
                    ).get();
    }

    public void deleteById(String id) throws ExecutionException, InterruptedException {
        client.query(Delete(Ref(Collection(COLLECTION),Value(id)))).get();
    }

    public void update(DeploymentModel updatedRecord) throws ExecutionException, InterruptedException {
        client.query(Update(Ref(Collection(COLLECTION), Value(updatedRecord.getId())),
                            Obj("data",
                                Obj("environment",
                                    Value(updatedRecord.getEnvironment()),
                                    "directory",
                                    Value(updatedRecord.getDirectory()),
                                    "ipAdd",
                                    Value(updatedRecord.getIpAddress())))))
              .get();
    }
}
