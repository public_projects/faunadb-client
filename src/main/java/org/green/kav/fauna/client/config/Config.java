package org.green.kav.fauna.client.config;

import com.faunadb.client.FaunaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Config {

    @Bean
    public FaunaClient getClient(){
        return
                FaunaClient.builder()
                           .withSecret("fnAD0mDb2cACBknhnV9FzNAHLlYqZdFStHTP65Rx")
                           .build();
    }
}
