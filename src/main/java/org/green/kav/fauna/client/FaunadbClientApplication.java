package org.green.kav.fauna.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FaunadbClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(FaunadbClientApplication.class, args);
	}

}
