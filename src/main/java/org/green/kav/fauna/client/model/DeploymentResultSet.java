package org.green.kav.fauna.client.model;

public class DeploymentResultSet {
    private String id;
    private DeploymentModel deployment;

    public DeploymentResultSet(){
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public DeploymentModel getDeployment() {
        return deployment;
    }

    public void setDeployment(DeploymentModel deployment) {
        this.deployment = deployment;
    }

    @Override
    public String toString() {
        return "DeploymentResultSet{" + "id='" + id + '\'' + ", deployment=" + deployment + '}';
    }
}
